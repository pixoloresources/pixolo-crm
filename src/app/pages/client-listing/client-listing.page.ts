import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-listing',
  templateUrl: './client-listing.page.html',
  styleUrls: ['./client-listing.page.scss'],
})
export class ClientListingPage implements OnInit {

  clientList: any = [];
  constructor(
    private apiService: ApiService,
    private dataService: DataService,
    public router: Router
  ) {
  }

  ngOnInit() {
  }

  ionViewWillEnter() {

    console.log(this.dataService.clientData);

    this.apiService.getAllData('projects').subscribe((data: any) => {
      console.log(data);

      this.clientList = data;
    }, (error) => {
      console.log("Internet error !");
    });

  }

  getInitial(name) {
    var str = name.split("");
    return str[0];
  }

  goToDashboard(client) {
    console.log(client);
    this.dataService.clientData = client;
    this.router.navigate(['/dashboard/' + client.client_id]);
  }

}
