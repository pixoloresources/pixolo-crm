import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClientListingPage } from './client-listing.page';

describe('ClientListingPage', () => {
  let component: ClientListingPage;
  let fixture: ComponentFixture<ClientListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientListingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClientListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
