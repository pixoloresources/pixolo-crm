import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-send-email',
  templateUrl: './send-email.component.html',
  styleUrls: ['./send-email.component.scss'],
})
export class SendEmailComponent implements OnInit {

  modaldata: any = {}
  constructor(
    private apiService: ApiService,
    private modalCtrl:ModalController
  ) { }

  ngOnInit() {
    console.log(this.modaldata);
  }

  sendMailDetails() {
    this.apiService.sendMailDetails(this.modaldata).subscribe((data: any) => {
      console.log(data);
      if(data){
        this.modaldata.summary = '';
        this.modaldata.upcoming_plan = '';
        this.modaldata.ps = '';
        this.modalCtrl.dismiss();
      }

    }, (error) => {
      console.log("Internet error !");
    });

  }


}
