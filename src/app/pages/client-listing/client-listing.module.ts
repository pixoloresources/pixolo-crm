import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClientListingPageRoutingModule } from './client-listing-routing.module';

import { ClientListingPage } from './client-listing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClientListingPageRoutingModule
  ],
  declarations: [ClientListingPage]
})
export class ClientListingPageModule {}
