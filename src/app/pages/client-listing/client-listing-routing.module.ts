import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientListingPage } from './client-listing.page';

const routes: Routes = [
  {
    path: '',
    component: ClientListingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientListingPageRoutingModule {}
