import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import { ModalController } from '@ionic/angular';
import { SendEmailComponent } from '../../components/send-email/send-email.component';
import { TaskListingComponent } from '../../components/task-listing/task-listing.component';
import { ApiService } from '../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  clientData: any = {};
  projectData: any = {};
  today: any = new Date();
  modalOpen: any = false;
  constructor(
    public modalCtrl: ModalController,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,

  ) {

    // this.projectData = {
    //   client_id: '',
    //   sheetname: 'Project now',
    //   start_date: '',
    //   id: 1,
    //   end_date: '2020-09-24',
    //   project_milestones: []
    // }

    // this.clientData = {
    //   id: '',
    //   name: '',
    //   company: '',
    //   contact: '',
    //   contact2: '',
    //   email: '',
    //   email2: ''
    // }
  }

  ngOnInit() {
  }


  ionViewWillEnter() {

    if (Object.keys(this.dataService.clientData).length === 0) {
      this.router.navigate(['/client-listing']);
    } else {

      this.projectData = this.dataService.clientData;

      this.projectData.initial = this.getInitial(this.projectData.sheetname);
      this.projectData.project_milestones = [];

      var id = this.route.snapshot.paramMap.get('id');

      this.apiService.getDataById('clients', id).subscribe((data: any) => {
        console.log(data);
        this.clientData = data;
        this.projectData.owner_name = this.clientData.name;
        this.projectData.email = this.clientData.email;
        this.projectData.email2 = this.clientData.email2;

        this.apiService.getDashboardData(this.projectData.sheetname).subscribe((data: any) => {
          console.log(data);
          this.reformatData(data);
        }, (error) => {
          console.log("Internet error !");
        });

      }, (error) => {
        console.log("Internet error !");
      });

    }



  }

  // project_milestones:[
  //   {
  //     'title': 'ABCD',
  //     'goals': [
  //       {
  //         'title': 'XYZ',
  //         'tasks': [
  //           {
  //             'title': 'task 1',
  //             'completed': 1
  //           }
  //         ]
  //       }
  //     ]
  //   }
  // ]
  // To take the Raw Excel as input and give out an Output in the above format.

  reformatData(excelData) {

    // Get the dynamic rows, because of this code below the Excel coluns can come in any order it wants. So usage will be as excelKeyIndex.client_milestone
    let excelKeyIndex = {
      'client_milestone': excelData[0].indexOf('Client Milestone'),
      'goal': excelData[0].indexOf('Development Milestones'),
      'task': excelData[0].indexOf('Required Tasks'),
      'timeline': excelData[0].indexOf('Timeline'),
      'asigned_to': excelData[0].indexOf('Asigned To'),
      'complete': excelData[0].indexOf('Completion'),
    };

    // Now we iterate from 1 to length of excelData and create Mile Stones and Goals, and put Tasks inside them.
    for (let i = 1; i < excelData.length; i++) {
      // We chaeck if that row is not empty. The API is returning only fields that is there, so defensive coding to check always is needed.
      if (excelData[i].length > 0) {

        // If it's a new Client Milestone a New Object will have to be added to the Array. If Not continue adding Goals/Tasks to the current Milestone
        if (excelData[i][excelKeyIndex.client_milestone] != "") {
          this.projectData.project_milestones.push({
            'title': this.checkAndAssign(excelData[i][excelKeyIndex.client_milestone]),
            'goals': []
          });
        }


        // console.log(this.projectData.project_milestones);


        // If it's a new Goal, Add Goal to the current Running Milestone. Or continue Adding Tasks.
        if (excelData[i][excelKeyIndex.goal] != "") {
          this.projectData.project_milestones[this.projectData.project_milestones.length - 1].goals.push(
            {
              'title': this.checkAndAssign(excelData[i][excelKeyIndex.goal]),
              'tasks': []
            }
          );
        }


        // Add the task to the last Goal straight Away
        this.projectData.project_milestones[this.projectData.project_milestones.length - 1].goals[this.projectData.project_milestones[this.projectData.project_milestones.length - 1].goals.length - 1].tasks.push(
          {
            'title': this.checkAndAssign(excelData[i][excelKeyIndex.task]),
            'completed': this.checkAndAssign(excelData[i][excelKeyIndex.complete]).toUpperCase() == "YES" ? 1 : 0
          }
        );


      }
    }
    // End of For Loop

    // Print Formated Data.
    console.log(this.projectData.project_milestones);

    this.addPercentage();
    setTimeout(() => { this.changePercentage(); }, 100);
  }

  // This function will have to be run on all Cells before inserting them to out Data
  checkAndAssign(excelCell) {
    if (excelCell) {
      return excelCell;
    }
    else {
      return 'Nothing Yet';
    }
  }

  formatData(excelData) {
    // console.log(excelData);
    var milestones = [];
    var project_milestones = [];

    for (var i = 1; i < excelData.length; i++) {
      if (milestones.indexOf(excelData[i][0]) == -1) {
        if (excelData[i][0]) {
          var obj = {
            name: excelData[i][0],
            goals1: [],
            goals2: [],
            goals: []
          }
          project_milestones.push(obj);
          milestones.push(excelData[i][0]);
        }
      }
      for (var j = 0; j < milestones.length; j++) {
        if (excelData[i][0] == milestones[j]) {
          project_milestones[j].goals1.push(excelData[i][1]);
          project_milestones[j].goals2 = Array.from(new Set(project_milestones[j].goals1));
        }
      }
    }

    for (var i = 0; i < project_milestones.length; i++) {
      for (var k = 0; k < project_milestones[i].goals2.length; k++) {
        var goal = {
          name: project_milestones[i].goals2[k]
        }
        project_milestones[i].goals.push(goal);
      }
      delete project_milestones[i].goals1;
      delete project_milestones[i].goals2;
    }


    for (var i = 0; i < project_milestones.length; i++) {
      for (var k = 0; k < project_milestones[i].goals.length; k++) {
        for (var l = 1; l < excelData.length; l++) {
          if (excelData[l][0] == project_milestones[i].name && excelData[l][1] == project_milestones[i].goals[k].name) {

            var task = {
              name: excelData[l][2],
              completion: ''
            }

            if (!excelData[l][5]) {
              task.completion = 'No';
            } else {
              task.completion = excelData[l][5];
            }

            // console.log(task);

            project_milestones[i].goals[k].tasks = [];
            project_milestones[i].goals[k].tasks.push(task);
          }
        }
      }
    }

    for (var i = 0; i < project_milestones.length; i++) {
      var count = 0;
      var tasklength = 0;
      for (var k = 0; k < project_milestones[i].goals.length; k++) {
        var goalscount = 0;
        var goalstasklength = 0;
        for (var l = 0; l < project_milestones[i].goals[k].tasks.length; l++) {
          if (project_milestones[i].goals[k].tasks[l].completion == 'Yes') {
            count += 1;
            goalscount += 1;
          }
          // console.log(project_milestones[i].goals[k].tasks[l]);
          tasklength += 1;
          goalstasklength += 1;
        }
        if (goalscount == goalstasklength) {
          project_milestones[i].goals[k].completion = 'Yes';
        } else {
          project_milestones[i].goals[k].completion = 'No';
        }
      }
      project_milestones[i].percentage = ((count / tasklength) * 100);
    }

    // console.log(project_milestones);
    // console.log(milestones);

    this.projectData.project_milestones = project_milestones;

    setTimeout(() => { this.changePercentage(); }, 100);

  }

  // FUNTION TO SHOW PERCENTAGE COMPLETION OF MILESTONES
  addPercentage() {

    var project_milestones = this.projectData.project_milestones;
    for (var i = 0; i < project_milestones.length; i++) {
      var count = 0;
      var tasklength = 0;
      for (var k = 0; k < project_milestones[i].goals.length; k++) {
        var goalscount = 0;
        for (var l = 0; l < project_milestones[i].goals[k].tasks.length; l++) {
          if (project_milestones[i].goals[k].tasks[l].completed == 1) {
            count += 1;
            goalscount += 1;
          }
          tasklength += 1;
        }
        project_milestones[i].goals[k].completed = Math.round((goalscount / project_milestones[i].goals[k].tasks.length) * 100);
      }
      project_milestones[i].percentage = Math.round((count / tasklength) * 100);
    }

    setTimeout(() => { this.changePercentage(); }, 100);

  }


  changePercentage() {

    var projectData = this.projectData;
    $(".progress-div").each(function (index) {
      var circle = $(this).children('.progress-svg').children('.colored-circle');
      var val = projectData.project_milestones[index].percentage;
      if (isNaN(val)) {
        val = 100;
      }
      else {
        var r = circle.attr('r');
        var c = Math.PI * (r * 2);
        if (val < 0) { val = 0; }
        if (val > 100) { val = 100; }
        var pct = ((100 - val) / 100) * c;
        circle.css({ strokeDashoffset: pct });
        $(this).attr('data-pct', val);
      }
    });

  }

  accordion(el, e) {

    e.preventDefault();

    let entityName = $(event.currentTarget).closest($('*').has('.accordion-header')).find('.accordion-header');

    if ((entityName).hasClass('accordion-header')) {
      $('.accordion-header').children('.btn-wrap').removeClass('rotate');
      if ((entityName).hasClass('active')) {
        $('.accordion-header').removeClass('active');
        $('.accordion-header').siblings('.accordion-content').slideUp(300);
      } else {
        $('.accordion-header').removeClass('active');
        $('.accordion-header').siblings('.accordion-content').slideUp(300);
        (entityName).addClass('active');
        (entityName).siblings('.accordion-content').slideDown(300);
        (entityName).children('.btn-wrap').addClass('rotate');
      }

    }

  }

  getInitial(name) {
    var str = name.split("");
    return str[0];
  }

  async sendMail() {
    this.modalOpen = true;
    const modal = await this.modalCtrl.create({
      component: SendEmailComponent,
      cssClass: 'send-mail-wrapper',
      componentProps: {
        modaldata: this.projectData,
      },
    });

    await modal.present();
    modal.onDidDismiss().then((data) => {
      // console.log(data);
      this.modalOpen = false;
    });
  }

  async viewTasks(tasks,goalName) {
    this.modalOpen = true;
    const modal = await this.modalCtrl.create({
      component: TaskListingComponent,
      cssClass: 'task-listing-modal',
      componentProps: {
        tasksList: tasks,
        goalName: goalName
      },
    });

    await modal.present();
    modal.onDidDismiss().then((data) => {
      // console.log(data);
      this.modalOpen = false;
    });
  }


  formateDate(dateValue) {
    // console.log(dateValue);
    var date = new Date(dateValue);

    var n = date.getDate();

    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var day = n + (n > 0 ? ['th', 'st', 'nd', 'rd'][(n > 3 && n < 21) || n % 10 > 3 ? 0 : n % 10] : '');

    if (monthNames[date.getMonth()]) {
      return day + ' ' + monthNames[date.getMonth()];
    } else {
      return 'NA';
    }



  }



  ionViewWillLeave() {
    if (this.modalOpen) {
      this.modalCtrl.dismiss();
    }

    this.dataService.clientData = {};

  }
}
