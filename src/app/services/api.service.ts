import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  adminurl: string = 'https://www.pixoloproductions.com/pixolocrm/rest/index.php/';

  constructor(
    private http: HttpClient
  ) { }


  getFormData(obj): any {
    let form = new FormData();
    for (let key in obj) {
      form.append(key, obj[key]);
    }
    return form;
  }

  getParams(obj): any {
    let params = new HttpParams();
    for (let key in obj) {
      params = params.append(key, obj[key]);
    }
    return params;
  }

  getAllData(tablename) {
    return this.http.get(this.adminurl + tablename + '/getall');
  }

  getDataById(tablename, id) {
    return this.http.get(this.adminurl + tablename + '/getbyid?id=' + id);
  }

  getDashboardData(project_name) {
    let obj = {
      project: project_name
    }
    return this.http.get('https://www.pixoloproductions.com/pixolocrm/gettasks.php', {
      params: this.getParams(obj)
    });
  }

  sendMailDetails(data) {
    
    var formData = new FormData();
    formData.append('data', JSON.stringify(data));
    return this.http.post('https://www.pixoloproductions.com/pixolocrm/mailer/mailer.php', formData);
  }

}
