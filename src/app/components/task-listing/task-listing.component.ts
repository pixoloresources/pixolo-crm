import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task-listing',
  templateUrl: './task-listing.component.html',
  styleUrls: ['./task-listing.component.scss'],
})
export class TaskListingComponent implements OnInit {

  tasksList: any = [];
  goalName: string = '';
  constructor() { }

  ngOnInit() {
    console.log(this.tasksList,this.goalName);
  }

}
