import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { SendEmailComponent } from './send-email/send-email.component';
import { FormsModule } from '@angular/forms';
import { TaskListingComponent } from './task-listing/task-listing.component';


@NgModule({
  declarations: [
    SendEmailComponent,
    TaskListingComponent
  ],
  entryComponents: [
    SendEmailComponent,
    TaskListingComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  exports: [
    SendEmailComponent,
    TaskListingComponent
  ]
})
export class ComponentmoduleModule { }
